# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "jenkins_exporter/map.jinja" import jenkins_exporter with context %}

{% for name, hosts in salt['pillar.get']('jenkins_exporter', {}).iteritems() %}
restart-jenkins-exporter-{{ name }}-service:
  service.running:
    - name: jenkins_exporter_{{ name }}.service
    - enable: True
    - restart: True
    - watch:
      - file: /etc/systemd/system/jenkins_exporter_{{ name }}.service
{% endfor %}
