# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "jenkins_exporter/map.jinja" import jenkins_exporter with context %}

{{jenkins_exporter.pip_package}}:
  pkg.installed

jenkins-exporter-create-user:
  user.present:
    - name: {{ jenkins_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

jenkins-exporter-create-group:
  group.present:
    - name: {{ jenkins_exporter.service_group }}
    - members:
      - {{ jenkins_exporter.service_user }}
    - require:
      - user: {{ jenkins_exporter.service_user }}

jenkins-exporter-dir:
  file.directory:
    - name: /opt/jenkins-exporter
    - user: {{ jenkins_exporter.service_user }}
    - group: {{ jenkins_exporter.service_group }}

jenkins-exporter-copy-executable:
  file.managed:
    - name: /opt/jenkins-exporter/jenkins_exporter.py
    - source: salt://jenkins_exporter/files/jenkins_exporter.py
    - user: {{ jenkins_exporter.service_user }}
    - group: {{ jenkins_exporter.service_group }}
    - mode: 0755

jenkins-exporter-copy-requiresments:
  file.managed:
    - name: /opt/jenkins-exporter/requirements.txt
    - source: salt://jenkins_exporter/files/jenkins_exporter_requirements.txt
    - user: {{ jenkins_exporter.service_user }}
    - group: {{ jenkins_exporter.service_group }}
    - mode: 0644

requirements:
    pip.installed:
        - requirements: /opt/jenkins-exporter/requirements.txt

{% for name, hosts in salt['pillar.get']('jenkins_exporter', {}).iteritems() %}
jenkins-exporter-create-{{ name }}-service:
  file.managed:
    - name: /etc/systemd/system/jenkins_exporter_{{ name }}.service
    - source: salt://jenkins_exporter/files/jenkins_exporter.service.j2
    - template: jinja
    - context:
        jen_exporter: {{ hosts }}
{% endfor %}
